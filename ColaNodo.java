public class ColaNodo
{
    public Nodo cabecera, cola;
    
        public ColaNodo() {
            cabecera = null;
            cola     = null;
        }
        
        public Nodo getCabecera() {
            return cabecera;
        }
        
        public Nodo getCola() {
            return cola;
        }
        
        public void insert(Nodo n) {
            if(cabecera == null){
                cabecera = n;
                cola = n;
            }else{
                if(cabecera.getSig() == null){
                    cabecera.setSig(n);
                    cola = n;
                }else{
                    cola.setSig(n);
                    cola = n;
                }
            }
        }
        
        public void reiniciar(){
            cabecera = null;
            cola     = null;
        }
}
