import java.awt.*;

/** Una clase para almacenar un punto (x, y) y un radio, mas un metodo draw
 *
 *  From "http://courses.coreservlets.com/Course-Materials/"
 *  coreservlets.com tutoriales sobre servlets, JSP, Struts, JSF, Ajax, GWT, and Java.
 */

public class SimpleCircle {
    private int x, y, radius;
    private String nombre;

    public SimpleCircle(int x, int y, int radius,String nombr) {
        setX(x);
        setY(y);
        setRadius(radius);
        nombre = nombr;
    }

    /** Dado un grafico, dibujar un SimpleCircle
     *  centrado alrededor de su posicion actual.
     */
    
    public void draw(Graphics g) {
        Graphics2D g2     = (Graphics2D) g;
        AlphaComposite ac = AlphaComposite.getInstance(AlphaComposite.SRC_IN, 0.5f);
        g2.setColor(Color.RED);
        g2.setComposite(ac);
   
        // g.setColor(Color.red);
        g.drawOval(x - radius, y - radius, radius * 2, radius * 2);
        g.drawString(nombre, x - 10,y - 10); 
        g.setColor(Color.black);
        g.fillOval(x - radius, y - radius, radius * 2, radius * 2);
    }

    public int getX() { return(x); }

    public void setX(int x) { this.x = x; }

    public int getY() { return(y); }

    public void setY(int y) { this.y = y; }

    public int getRadius() { return(radius); }

    public void setRadius(int radius) {
        this.radius = radius;
    }
}
