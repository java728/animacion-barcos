import java.awt.*;

public class Nodo
{
    private Point punto;
    private Nodo siguiente;
    
        public Nodo(Point p,Nodo s){
            setPunto(p);
            setSig(s);
        }
        
        public void setPunto(Point p){
            punto = p;
        }
        
        public void setSig(Nodo s){
            siguiente = s;
        }
        
        public Point getPunto(){
            return punto;
        }
        
        public Nodo getSig(){
            return siguiente;
        }
}
