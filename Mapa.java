import java.awt.geom.*;
import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.awt.image.BufferedImage;
import java.awt.image.*;
import java.util.concurrent.*;
import java.net.URL;

public class Mapa extends JFrame implements Runnable
{
    private ArrayList<SimpleCircle> circles;
    private ColaNodo cn;
    private volatile boolean running;

    private Image imagen;
    private Image imagenBarco2;
    private Image imagenBarco3;
    private Image imagenBarco4;
    private Image imagenBarco5;
      
    /** Contexto grafico para dibujar una imagen e identificador
     * para contenerla y dibujarla en la pantalla.
     * Es parte del proceso de crear una imagen off-screen, es decir
     * una imagen que no proviene de una archivo.
     */
    private Graphics grPuntos;
    private Image imagenPuntos;
        
    private Graphics grMapa;
    private Image imagenMapa;
    private Thread hilo;
        
    /** Puertos almacenados en la lista para usarlos en la animacion run() */
    Nodo n,n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14,n15,n16,n17;
    
    /** Ubicaciones de los puertos en la imagen de la Rep Mex */
    int xp;
    int yp;
    int xp1,yp1, xp2,yp2, xp3,yp3, xp4,yp4, xp1regreso, yp1regreso,
        xp5,yp5, xp6,yp6, xp7,yp7, xp8,yp8, xp9,yp9;
     private boolean listaVacia=false;
     private boolean reiniciar=false;
    public Mapa()
    {       
        Toolkit t    = Toolkit.getDefaultToolkit ();
        imagen       = t.getImage ("barquito.png");
        imagenBarco2 = t.getImage ("barco2.png");
        imagenBarco3 = t.getImage ("barco3.png");
        imagenBarco4 = t.getImage ("barco4.png");
        imagenBarco5 = t.getImage ("barco5.png");
        
        circles      = new ArrayList<SimpleCircle>();
        cn           = new ColaNodo();
        running      = true;
        this.setSize(900,700);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        
        /** Iniciar el metodo run */
        iniciarH();
    }
   
    public void run() {
         while(running) {   
            moverBarcos();
        }
    }
    public void iniciarH(){
        hilo = new Thread(this);
        hilo.start();
    }
    
    public void moverBarcos(){
        pause(3000);
        for(int g = xp; g >= n.getPunto().x - 30;g--)
        {            
            pause(80);
            
            xp  -= 1;
            yp1 -= 1;
             
            if(xp3 >= n1.getPunto().x - 30)
            {
                xp3 -= 1;
            }
            yp3 -= 1;
            yp--;
            
            if(xp4 >= n7.getPunto().x - 30)
            {
                xp4 -= 1;
            }
            else { yp4--;
            }  
             
            if(yp5 >= n16.getPunto().y - 30)
            {
                yp5 -= 1;
            }
            
            if(xp5 < n16.getPunto().x - 20)
            {
                xp5 += 1;
            }
                                
            if(yp6 >= n10.getPunto().y - 30)
            {
                yp6 -= 1;
            }
            
            if(xp6 > n10.getPunto().x - 20 && yp6 <= n10.getPunto().y - 30)
            {
                xp6 -= 1;
            }
                                
            if(yp7 >= n15.getPunto().y - 30)
            {
                yp7 -= 1;
            }
            
            if(xp7 < n15.getPunto().x - 20 )
            {
                xp7 += 1;
            }
                              
            if(yp8 >= n2.getPunto().y - 30)
            {
                yp8 -= 1;
            }
            
            if(xp8 >= n2.getPunto().x - 10)
            {
                xp8 -= 1;
            }
            
            if(yp2 >= n3.getPunto().y - 20)
            {
                yp2 -= 1;
            }
            
            if(xp2 >= n3.getPunto().x - 30)
            {
                xp2 -= 1;
            }
                
            if(yp9 <= n4.getPunto().y - 40)
            {
                yp9 += 1;
            }
            
            if(xp2 <= n4.getPunto().x - 30 && yp9 >= n4.getPunto().y - 40)
            {
                xp9 += 1;
            }
                                
            if(yp1regreso <= n8.getPunto().y - 20)
            {
                yp1regreso += 1;
            }
            
            if(xp1regreso <= n8.getPunto().x - 30)
            {
                xp1regreso += 1;
            }                
            repaint();
        }
         
        for(int g1 = yp;g1 >= n.getPunto().y - 30;g1--)
        {
            pause(80);                
            yp -= 1;
            if(xp9 <= n4.getPunto().x - 30 && yp9 >= n4.getPunto().y - 40)
            {
                xp9 += 1;
            }
          
            if(yp3 > n1.getPunto().y - 30)
            {
                yp3 -= 1;                     
            }
            
            if(yp4 > n7.getPunto().y - 30)
            {
                yp4 -= 1;                     
            }
            repaint();
        }
        reiniciar=true;
    }
 
    public void paint(Graphics g)
    {       
        int w=getWidth();
        int h=getHeight();
        Image dibujoAux=createImage(w,h); 
        Graphics2D gAux=(Graphics2D)dibujoAux.getGraphics();
        /** Crea la imagen off-screen de la Rep Mex */
        pintarMapa(gAux);
        if(circles.isEmpty()){/** Crea la lista de nodos de los puertos mexicanos */ 
        llenarPuntos(gAux);
        listaVacia=true;
       }
        
        /** Dibuja el mapa de la Rep Mex */
        gAux.drawImage(imagenMapa,0,0,this);
        
        /** Ubica las imagenes de los barcos en los puertos */
        if(listaVacia|| reiniciar){
        iniciar();
        listaVacia=false;
        reiniciar=false;
      }
        /** Dibuja los barcos atracados en los puertos */
        gAux.drawImage(imagenBarco5,xp,yp,this);
        gAux.drawImage(imagenBarco4,xp2,yp2,this);      
        gAux.drawImage(imagen,xp3,yp3,this);
        gAux.drawImage(imagenBarco2,xp4,yp4,this);
        gAux.drawImage(imagenBarco3,xp1regreso,yp1regreso,this);
        gAux.drawImage(imagenBarco3,xp5,yp5,this);
        gAux.drawImage(imagen,xp6,yp6,this);
        gAux.drawImage(imagenBarco3,xp7,yp7,this);
        gAux.drawImage(imagenBarco2,xp8,yp8,this);
        gAux.drawImage(imagenBarco5,xp9,yp9,this);
        g.drawImage(dibujoAux,0,0,this);
    }
    
    public void llenarPuntos(Graphics g) {
        imagenPuntos  = createImage(1000,700);
        grPuntos      = (Graphics2D) imagenPuntos.getGraphics();
        Graphics2D g2 = (Graphics2D) grPuntos;
        
        AlphaComposite ac = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.1f);
        g2.setColor(Color.black);
        g2.setComposite(ac);
      
        n = new Nodo(new Point(20,44),null);
        circles.add(new SimpleCircle(n.getPunto().x,n.getPunto().y, 5,"Ensenada"));
        cn.insert(n);
                           
        n1 = new Nodo(new Point(48,185),null);
        circles.add(new SimpleCircle(n1.getPunto().x,n1.getPunto().y, 5,"Cedro"));
        cn.insert(n1);  
                            
        n2 = new Nodo(new Point(100,170),null);
        circles.add(new SimpleCircle(n2.getPunto().x,n2.getPunto().y, 5,"San Marcos"));
        cn.insert(n2);                      
        
        n3 = new Nodo(new Point(188,230),null);
        circles.add(new SimpleCircle(n3.getPunto().x,n3.getPunto().y, 5,"Guaymas"));
        cn.insert(n3);                   
        
        n4 = new Nodo(new Point(273,360),null);
        circles.add(new SimpleCircle(n4.getPunto().x,n4.getPunto().y, 5,"Mazatlan"));
        cn.insert(n4);         
                            
        n5 = new Nodo(new Point(285,435),null);
        circles.add(new SimpleCircle(n5.getPunto().x,n5.getPunto().y, 5,"Puerto Vallarta"));
        cn.insert(n5);                   
        
        n6 = new Nodo(new Point(297,470),null);
        circles.add(new SimpleCircle(n6.getPunto().x,n6.getPunto().y, 5,"Manzanillo"));
        cn.insert(n6);                   
        
        n7 = new Nodo(new Point(387,515),null);
        circles.add(new SimpleCircle(n7.getPunto().x,n7.getPunto().y, 5,"Lazaro Cardenas"));
        cn.insert(n7);        
                            
        n8 = new Nodo(new Point(460,555),null);
        circles.add(new SimpleCircle(n8.getPunto().x,n8.getPunto().y, 5,"Acapulco"));
        cn.insert(n8);                   
                            
        n9 = new Nodo(new Point(600,560),null);
        circles.add(new SimpleCircle(n9.getPunto().x,n9.getPunto().y, 5,"Salina Cruz"));
        cn.insert(n9);   
                            
        n10 = new Nodo(new Point(505,340),null);
        circles.add(new SimpleCircle(n10.getPunto().x,n10.getPunto().y, 5,"Altamira"));
        cn.insert(n10);                   
                            
        n11 = new Nodo(new Point(505,370),null);
        circles.add(new SimpleCircle(n11.getPunto().x,n11.getPunto().y, 5,"Tampico"));
        cn.insert(n11);                

        n12 = new Nodo(new Point(515,405),null);
        circles.add(new SimpleCircle(n12.getPunto().x,n12.getPunto().y, 5,"Tuxpan"));
        cn.insert(n12);                    
        
        n13 = new Nodo(new Point(555,470),null);
        circles.add(new SimpleCircle(n13.getPunto().x,n13.getPunto().y, 5,"Veracruz"));
        cn.insert(n13);                
        
        n14 = new Nodo(new Point(608,500),null);
        circles.add(new SimpleCircle(n14.getPunto().x,n14.getPunto().y, 5,"Coatzacoalcos"));
        cn.insert(n14);      
            
        n15 = new Nodo(new Point(668,480),null);
        circles.add(new SimpleCircle(n15.getPunto().x,n15.getPunto().y, 5,"Dos Bocas"));
        cn.insert(n15);                  
        
        n16 = new Nodo(new Point(745,385),null);
        circles.add(new SimpleCircle(n16.getPunto().x,n16.getPunto().y, 5,"Progreso"));
        cn.insert(n16);                 
        
        n17 = new Nodo(new Point(800,420),null);
        circles.add(new SimpleCircle(n17.getPunto().x,n17.getPunto().y, 5,"Punta venados"));
        cn.insert(n17);
        
        for(SimpleCircle circle: circles)
        {
            circle.draw(grPuntos);                  
        }
    }
        
    private void pause(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch(InterruptedException ie) {}
    }

    public void iniciar() {
        xp  = n6.getPunto().x;
        yp  = n6.getPunto().y;
        xp5 = n13.getPunto().x;
        yp5 = n13.getPunto().y;
        xp1 = xp;
        yp1 = yp;
        xp2 = xp - 30;
        yp2 = yp - 10;
        xp3 = xp - 40;
        yp3 = yp + 10;
        xp4 = n9.getPunto().x;
        yp4 = n9.getPunto().y;
        xp1regreso = n6.getPunto().x - 20;
        yp1regreso = n6.getPunto().y - 20;          
        xp6 = xp5 - 20;
        yp6 = yp5 - 20;
        xp7 = xp5 - 10;
        yp7 = yp5 - 10;
        xp5 -= 20;
        yp5 -= 20;
        xp8 = n4.getPunto().x - 20;
        yp8 = n4.getPunto().y - 30;
        xp9 = n1.getPunto().x - 35;
        yp9 = n1.getPunto().y - 30;
    }

    public void pintarMapa(Graphics g) {
        imagenMapa    = createImage(1000,700);
        grMapa        = (Graphics2D) imagenMapa.getGraphics();
        Graphics2D g2 = (Graphics2D) grMapa;
        
        g2.setColor(Color.black);
        g2.setStroke(new BasicStroke(3.0F));
        GeneralPath gp = new GeneralPath();
        gp.moveTo(17,34);
        gp.lineTo(76,34);
        gp.lineTo(76,39);
        gp.lineTo(74,41);
        gp.lineTo(163,93);
        gp.lineTo(236,93);
        gp.lineTo(236,80);
        gp.lineTo(264,80);
        gp.lineTo(271,84);
        gp.lineTo(278,85);
        gp.curveTo(280,86,282,89,283,94);
        gp.curveTo(287,97,291,99,295,104);
        gp.curveTo(297,109,304,115,317,124);
        gp.curveTo(320,141,326,154,341,162);
        gp.curveTo(347,165,352,170,358,172);
        gp.curveTo(362,168,366,162,368,154);
        gp.curveTo(379,148,392,144,405,148);
        gp.curveTo(408,153,412,158,416,159);
        gp.curveTo(424,169,426,181,434,189);
        gp.lineTo(434,194);
        gp.lineTo(440,200);
        gp.lineTo(443,208);
        gp.curveTo(448,209,453,213,454,217);
        gp.curveTo(457,230,461,240,465,247);
        gp.curveTo(475,250,482,255,489,256);
        gp.lineTo(493,258);
        gp.lineTo(507,258);
        gp.lineTo(510,261);
        gp.lineTo(516,261);
        gp.curveTo(518,269,510,279,510,282);
        gp.lineTo(501,284);
        gp.lineTo(503,287);
        gp.lineTo(503,300);
        gp.lineTo(505,302);
        gp.curveTo(502,319,502,340,506,353);
        gp.lineTo(506,360);
        gp.curveTo(504,363,503,366,504,369);
        gp.lineTo(504,381);
        gp.curveTo(508,385,512,392,515,395);
        gp.curveTo(516,399,514,399,509,395);
        gp.curveTo(510,401,514,406,517,408);
        gp.curveTo(518,416,524,425,534,436);
        gp.curveTo(537,442,543,450,547,459);
        gp.curveTo(550,465,555,471,561,476);
        gp.curveTo(568,487,572,488,574,484);
        gp.curveTo(583,485,589,487,595,488);
        gp.lineTo(602,496);
        gp.curveTo(604,496,606,498,607,501);
        gp.lineTo(616,502);
        gp.curveTo(619,498,622,497,625,499);
        gp.lineTo(633,492);
        gp.lineTo(643,492);
        gp.lineTo(653,487);
        gp.lineTo(666,477);
        gp.lineTo(680,477);
        gp.curveTo(683,483,690,482,697,479);
        gp.curveTo(701,477,702,475,696,469);
        gp.curveTo(710,455,715,431,714,406);
        gp.curveTo(730,393,744,386,760,383);
        gp.curveTo(764,380,769,380,770,378);
        gp.curveTo(775,378,778,375,785,375);
        gp.curveTo(789,373,794,376,799,377);
        gp.curveTo(806,371,810,370,814,374);
        gp.curveTo(816,381,814,388,814,395);
        gp.curveTo(808,403,803,411,802,422);
        gp.curveTo(804,425,801,431,799,436);
        gp.lineTo(805,437);
        gp.lineTo(805,440);
        gp.lineTo(800,447);
        gp.lineTo(805,447);
        gp.lineTo(805,455);
        gp.curveTo(802,463,800,471,800,478);
        gp.curveTo(798,481,795,476,792,472);
        gp.lineTo(792,461);
        gp.curveTo(787,464,786,471,784,472);
        gp.curveTo(780,474,776,479,775,486);
        gp.lineTo(769,492);
        gp.lineTo(761,491);
        gp.lineTo(760,496);
        gp.lineTo(713,504);
        gp.lineTo(713,521);
        gp.curveTo(707,521,704,520,703,523);
        gp.lineTo(713,530);
        gp.curveTo(719,532,722,534,726,543);
        gp.lineTo(731,543);
        gp.lineTo(732,557);
        gp.lineTo(694,557);
        gp.lineTo(682,583);
        gp.lineTo(686,588);
        gp.curveTo(685,592,685,597,687,601);
        gp.curveTo(684,604,683,609,680,609);
        gp.lineTo(671,597);
        gp.lineTo(666,596);
        gp.lineTo(646,579);
        gp.curveTo(642,574,637,572,631,572);
        gp.curveTo(628,567,623,563,616,560);
        gp.curveTo(612,564,608,564,607,559);
        gp.curveTo(599,558,599,562,590,564);
        gp.lineTo(579,574);
        gp.lineTo(574,574);
        gp.lineTo(560,583);
        gp.curveTo(557,585,553,586,549,584);
        gp.curveTo(541,580,532,577,523,577);
        gp.curveTo(520,576,517,576,514,576);
        gp.lineTo(490,564);
        gp.curveTo(483,560,472,556,455,554);
        gp.curveTo(444,550,434,545,422,544);
        gp.lineTo(399,528);
        gp.lineTo(389,516);
        gp.lineTo(371,517);
        gp.curveTo(363,515,357,510,343,507);
        gp.curveTo(339,497,312,478,301,476);
        gp.curveTo(298,467,291,459,287,447);
        gp.curveTo(284,442,285,435,294,434);
        gp.curveTo(290,429,290,422,296,417);
        gp.lineTo(296,404);
        gp.curveTo(291,400,287,389,284,375);
        gp.curveTo(288,372,280,365,278,359);
        gp.lineTo(267,356);
        gp.lineTo(243,316);
        gp.curveTo(237,316,234,311,237,308);
        gp.curveTo(233,304,227,301,225,297);
        gp.lineTo(225,286);
        gp.lineTo(213,281);
        gp.lineTo(207,273);
        gp.lineTo(200,273);
        gp.lineTo(200,267);
        gp.lineTo(193,267);
        gp.curveTo(190,263,189,254,193,246);
        gp.curveTo(196,250,200,248,196,245);
        gp.curveTo(193,237,183,233,178,228);
        gp.curveTo(178,224,174,221,170,220);
        gp.curveTo(165,216,165,208,163,202);
        gp.curveTo(163,197,153,194,148,189);
        gp.curveTo(143,180,137,177,129,163);
        gp.curveTo(128,151,118,130,110,108);
        gp.curveTo(108,97,111,88,105,83);
        gp.curveTo(99,84,97,78,93,73);
        gp.curveTo(87,74,79,71,77,65);
        gp.lineTo(69,60);
        gp.curveTo(69,69,70,76,66,80);
        gp.curveTo(66,84,69,90,69,105);
        gp.curveTo(68,115,71,122,78,129);
        gp.lineTo(90,144);
        gp.lineTo(90,156);
        gp.lineTo(100,161);
        gp.lineTo(100,171);
        gp.curveTo(107,176,107,184,107,187);
        gp.curveTo(112,195,117,199,118,210);
        gp.lineTo(126,217);
        gp.curveTo(123,219,122,225,129,224);
        gp.curveTo(137,239,137,257,146,268);
        gp.lineTo(145,277);
        gp.curveTo(149,281,150,285,153,289);
        gp.curveTo(154,294,151,296,154,303);
        gp.curveTo(151,309,158,312,160,308);
        gp.curveTo(163,307,176,321,177,329);
        gp.curveTo(188,333,183,342,173,352);
        gp.curveTo(168,354,161,349,161,340);
        gp.curveTo(154,328,144,318,131,308);
        gp.curveTo(128,302,123,299,119,296);
        gp.curveTo(118,292,114,289,113,272);
        gp.lineTo(116,268);
        gp.lineTo(115,253);
        gp.curveTo(113,248,109,241,105,241);
        gp.lineTo(93,228);
        gp.lineTo(93,221);
        gp.curveTo(91,220,88,219,84,219);
        gp.lineTo(82,223);
        gp.lineTo(70,214);
        gp.curveTo(68,209,65,207,62,207);
        gp.curveTo(62,203,59,199,54,198);
        gp.lineTo(46,184);
        gp.curveTo(54,185,60,188,65,185);
        gp.curveTo(68,187,71,189,71,184);
        gp.curveTo(75,181,75,175,76,169);
        gp.curveTo(71,157,62,145,53,133);
        gp.curveTo(48,132,46,128,40,123);
        gp.lineTo(38,103);
        gp.lineTo(34,99);
        gp.lineTo(33,86);
        gp.lineTo(29,83);
        gp.lineTo(29,72);
        gp.lineTo(23,65);
        gp.lineTo(23,57);
        gp.curveTo(25,55,23,51,21,49);
        gp.closePath();
 
        g2.drawImage(imagenPuntos,0,0,this);
         
        g2.draw(gp);
        
        AlphaComposite ac = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f);
        g2.setColor(new Color(128,255,0));
        g2.setComposite(ac);
        g2.fill(gp);
        
        AlphaComposite ac1 = AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, 0.1f);
        g2.setColor(new Color(0,0,255));
        g2.setComposite(ac1);
        g2.fillRect(0,0,1500,1500);
      
        g2.setFont(new Font("Times New Roman",Font.BOLD,20));
        AffineTransform rotacion1 = AffineTransform.getRotateInstance(3,1.5);
        g2.setTransform(rotacion1);
        g2.setColor(Color.black);
        g2.drawString(" OCEANO PACIFICO",300,385);
            
        g2.setFont(new Font("Times New Roman",Font.BOLD,15));
        g2.setTransform(rotacion1);
        g2.drawString("GOLFO DE MEXICO",670,100);
         
        g2.setComposite(ac);
        AffineTransform rotacion11 = AffineTransform.getRotateInstance(8,0);
        g2.setTransform(rotacion11);
        g2.setFont(new Font("Times New Roman",Font.BOLD,24));
        g2.drawString(" PRINCIPALES PUERTOS  DE  LA  REPUBLICA MEXICANA",150,75);
    }
     public void audio(){
         String path="Supermercado.wav";
         URL url= this.getClass().getResource(path);
         StdAudio.loop(path);
    }
    public static void main(String[] args) {
        Mapa rep = new Mapa();
        rep.audio();
    }
}
